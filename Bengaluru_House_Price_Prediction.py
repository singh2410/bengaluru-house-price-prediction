#!/usr/bin/env python
# coding: utf-8

# # Bengaluru House Price Prediction
# #By- Aarush Kumar
# #Dated: August 07,2021

# In[1]:


import numpy as np 
import pandas as pd
from matplotlib import pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib 
matplotlib.rcParams["figure.figsize"] = (20,10)
import seaborn as sns
from sklearn import preprocessing
from sklearn import model_selection
import sklearn
import xgboost


# In[2]:


home = pd.read_csv("/home/aarush100616/Downloads/Projects/Bengaluru House Price Prediction/Bengaluru_House_Data.csv")
home.head()


# In[3]:


home.info()


# In[4]:


round(100*(home.isnull().sum()/len(home.index)),2)


# In[5]:


home.dropna(inplace =True)


# In[6]:


home = home.drop(columns='society')


# In[7]:


home.reset_index(drop= True, inplace =True)


# In[8]:


home['bhk'] = home['size'].str.split().str[0]
home['bhk'].dropna(inplace = True)
home['bhk'] = home['bhk'].astype('int')


# In[9]:


print(home['total_sqft'].iloc[[17]])


# In[10]:


def convert_sqft_to_num(x):
    tokens = x.split('-')
    if len(tokens) == 2:
        return (float(tokens[0])+float(tokens[1]))/2
    try:
        return float(x)
    except:
        return None


# In[11]:


home.total_sqft = home.total_sqft.apply(convert_sqft_to_num)
home = home[home.total_sqft.notnull()]
home.head(2)


# In[12]:


home = home[~(home.total_sqft/home.bhk<200)]
home.shape


# In[13]:


cont_ = home.select_dtypes(exclude = 'object')
cat_ = home.select_dtypes(include  = 'object')


# In[14]:


fig = plt.figure(figsize = (10,8))
for index,col in enumerate(cont_):
    plt.subplot(3,2,index+1)
    sns.boxplot(y = cont_.loc[:,col])
fig.tight_layout(pad = 1.0)


# In[15]:


home = home.drop(home[home['bath']>6].index)
home = home.drop(home[home['bhk']>7.0].index)


# In[16]:


home['price_per_sqft'] = home['price']*100000/home['total_sqft']
home.head()


# In[17]:


home['price_per_sqft'].describe()


# In[18]:


def remove_pps_outliers(df):
    df_out = pd.DataFrame()
    for key, subdf in df.groupby('location'):
        m = np.mean(subdf.price_per_sqft)
        st = np.std(subdf.price_per_sqft)
        reduced_df = subdf[(subdf.price_per_sqft>(m-st)) & (subdf.price_per_sqft<=(m+st))]
        df_out = pd.concat([df_out,reduced_df],ignore_index=True)
    return df_out
home = remove_pps_outliers(home)
home.shape


# In[19]:


corr = home.corr()
plt.figure(figsize = (10,8))
sns.heatmap(corr,mask = corr<0.8 ,annot= True,cmap = 'Blues')


# In[20]:


home.drop(columns=['availability','size','area_type'],inplace = True)


# In[21]:


home.location = home.location.str.strip()
location_stats = home['location'].value_counts(ascending=False)
location_stats


# In[22]:


location_stats_less_than_10 = location_stats[location_stats<=10]
location_stats_less_than_10


# In[23]:


home.location = home.location.apply(lambda x: 'other' if x in location_stats_less_than_10 else x)
home = home[home.location != 'other']


# In[24]:


home = home[home.bath<home.bhk+2]


# In[25]:


num_ = home.select_dtypes(exclude = 'object')
fig = plt.figure(figsize =(10,8))
for index, col in enumerate(num_):
    plt.subplot(3,2,index+1)
    sns.distplot(num_.loc[:,col],kde = False)
fig.tight_layout(pad = 1.0)


# In[26]:


dummies = pd.get_dummies(home.location)
dummies.head(3)


# In[27]:


home = pd.concat([home,dummies],axis='columns')
home1 = home.drop('location',axis = 1)
home1 = home1.drop(columns=['balcony','price_per_sqft'])
home1


# In[28]:


home1.reset_index(drop = True)


# In[29]:


X = home1.drop('price',axis = 1).values 
y = home1.price.values 


# In[30]:


y = y[:,np.newaxis]


# In[31]:


sc = preprocessing.StandardScaler()
X1 = sc.fit_transform(X)


# In[32]:


Std_x1 = preprocessing.scale(X)


# In[33]:


from sklearn.model_selection import cross_val_score,cross_val_predict
from sklearn.linear_model import LinearRegression
lr = LinearRegression()
from sklearn.model_selection import cross_validate as CV


# In[34]:


cross1 = cross_val_score(lr,Std_x1,y,cv=5,scoring='neg_mean_squared_error')
print(cross1.mean())


# In[35]:


sklearn.metrics.SCORERS.keys()


# In[36]:


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X1,y,test_size=0.3,random_state=10)


# In[37]:


from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error,r2_score
lr.fit(X_train,y_train)
y_pred = lr.predict(X_test)
acc = mean_squared_error(y_pred,y_test)
rscore = r2_score(y_pred,y_test)
print(rscore)

